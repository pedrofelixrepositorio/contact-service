package com.claro.contact;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.claro.contact.model.Contact;
import com.claro.contact.repository.ContactRepository;
import com.claro.contact.service.ContactService;
import com.claro.contact.util.TestUtil;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(MockitoJUnitRunner.class)
class ContactControllerTests {

    private static final String CONTACT_RESOURCE_URL = "/api/contact";
    private static final String CONTACT_RESOURCE_URL_ID = CONTACT_RESOURCE_URL + "/{id}";

    @Autowired
    private ContactRepository contactRepository;
    
    @Autowired
    private ContactService contactService;

    @Autowired
    private MockMvc contactController;

    @Test
    void searchContact_Expect_Correct_Data() throws Exception {

        contactController.perform(get(CONTACT_RESOURCE_URL)).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(jsonPath("$.[*].email").value(hasItem("test2e@teste.com")))
        .andExpect(jsonPath("$.[*].name").value(hasItem("Teste 2")))
        .andExpect(jsonPath("$.[*].telephone").value(hasItem("11999991111")))
        .andExpect(jsonPath("$.[*].cep").value(hasItem("04287000")))
        .andExpect(jsonPath("$.[*].address").value(hasItem("Rua teste, 456")))
        .andExpect(jsonPath("$.[*].city").value(hasItem("São Paulo")))
        .andExpect(jsonPath("$.[*].uf").value(hasItem("SP")));

    }

    @Test
    void creatingContactByPassingZipCodeAtTheAddress_Expect_Correct_Data() throws Exception {
        Contact contact = createContactWithZipCodeAtTheAddress();

        contactController.perform(post(CONTACT_RESOURCE_URL).contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(contact))).andExpect(status().isCreated());
        Optional<Contact> contactSaved = contactRepository.findById(contact.getEmail());

        assertThat(contactSaved.get().getEmail()).isEqualTo("pedro@claro.com");
        assertThat(contactSaved.get().getCity()).isEqualTo("São José de Mipibu");
        assertThat(contactSaved.get().getUf()).isEqualTo("RN");

    }

    @Test
    void updateContact_Expect_Correct_Data() throws Exception {
        Contact contact = createContactWithZipCodeAtTheAddress();

        Contact contactSaved = contactService.saveContact(contact);

        assertThat(contactSaved.getEmail()).isEqualTo("pedro@claro.com");

        contactSaved.setTelephone("99999999");
        contactSaved.setName("Felix");

        contactController.perform(put(CONTACT_RESOURCE_URL_ID, contactSaved.getEmail())
                .contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(contactSaved)))
                .andExpect(status().isOk());

        assertThat(contactSaved.getTelephone()).isEqualTo("99999999");
        assertThat(contactSaved.getName()).isEqualTo("Felix");

    }

    @Test
    void saveContactEndThenDelete_Expect_No_content() throws Exception {
        Contact contact = createContactWithZipCodeAtTheAddress();
        Contact contactSaved = contactService.saveContact(contact);

        contactController
                .perform(delete(CONTACT_RESOURCE_URL_ID, contactSaved.getEmail())
                        .contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(contact)))
                .andExpect(status().isNoContent());

    }

    public Contact createContactWithZipCodeAtTheAddress() {

        Contact contact = new Contact();
        contact.setCep("59162000");
        contact.setName("Pedro");
        contact.setEmail("pedro@claro.com");
        contact.setTelephone("123");

        return contact;

    }

}
