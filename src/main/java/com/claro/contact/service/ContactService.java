package com.claro.contact.service;

import java.util.List;
import java.util.Optional;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.claro.contact.model.Address;
import com.claro.contact.model.Contact;
import com.claro.contact.repository.ContactRepository;

@Service
public class ContactService {

    private static final String DEFAULT_EXCEPTION_MESSAGE_FOR_NON_EXISTENT_CONTACT_ID = "Contact id does not exists";

    private ContactRepository contactRepository;

    public ContactService(ContactRepository contactRepository) {
        this.contactRepository = contactRepository;
    }

    public Contact saveContact(Contact contact) {
        Address address = searchAddress(contact.getCep());
        contact.setCity(address.getCity());
        contact.setUf(address.getState());
        contact.setAddress(address.getAddress());
        
        return contactRepository.save(contact);

    }

    public Contact updateContact(String id, Contact contact) {
        contactRepository.findById(id).ifPresentOrElse(contactFound -> {
            contactFound.setAddress(contact.getAddress());
            contactFound.setCep(contact.getCep());
            contactFound.setCity(contact.getCity());
            contactFound.setName(contact.getName());
            contactFound.setTelephone(contact.getTelephone());
            contactFound.setEmail(contact.getEmail());
            contactFound.setUf(contact.getUf());

            contactRepository.saveAndFlush(contactFound);

        }, () -> {
            throw new IllegalArgumentException(DEFAULT_EXCEPTION_MESSAGE_FOR_NON_EXISTENT_CONTACT_ID);
        });

        return this.findById(id);
    }

    public List<Contact> getAllContacts() {
        return contactRepository.findAll();
    }

    public void deleteContact(String id) {
        contactRepository.deleteById(id);
    }

    public Optional<Contact> findByIdContact(String id) {
        return contactRepository.findById(id);
    }
    
    public Address searchAddress(String code) {

        RestTemplate restTemplate = new RestTemplate();
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        restTemplate = restTemplateBuilder.build();

        Address address = restTemplate.getForObject("https://ws.apicep.com/cep/" + code + ".json", Address.class);

        return address;

    }

    public Contact findById(String id) {
        return contactRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException(DEFAULT_EXCEPTION_MESSAGE_FOR_NON_EXISTENT_CONTACT_ID));
    }

}
