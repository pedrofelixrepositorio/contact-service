package com.claro.contact.model;

public class Address {
    
    private String code;
    
    private String state;
    
    private String city;
    
    private String district;
    
    private String address;
    
    private String statusText;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    @Override
    public String toString() {
        return "Address [code=" + code + ", state=" + state + ", city=" + city + ", district=" + district + ", address="
                + address + ", statusText=" + statusText + "]";
    }

}
