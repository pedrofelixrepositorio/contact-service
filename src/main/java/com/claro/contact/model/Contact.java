package com.claro.contact.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_contato") 
public class Contact {
    
    @Id
    @Column(name ="email")
    private String email;
    
    @Column(name ="nome")
    private String name;
    
    @Column(name ="telefone")
    private String telephone;
    
    @Column(name ="cep")
    private String cep;
    
    @Column(name ="endereco")
    private String address;
    
    @Column(name ="cidade")
    private String city;
    
    @Column(name ="uf")
    private String uf;
    
    @Column(name ="dh_cadastro")
    private Date dhRegistration = new Date();

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public Date getDhRegistration() {
        return dhRegistration;
    }

    public void setDhRegistration(Date dhRegistration) {
        this.dhRegistration = dhRegistration;
    }
    
    

}
