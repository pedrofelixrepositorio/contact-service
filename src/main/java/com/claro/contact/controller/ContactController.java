package com.claro.contact.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;

import com.claro.contact.model.Contact;
import com.claro.contact.service.ContactService;

@RestController
@RequestMapping("api/contact")
public class ContactController {
    
    private ContactService contactService;

    public ContactController(ContactService contactService) {
        this.contactService = contactService;
    }
    
    @PostMapping()
    public ResponseEntity<Contact> saveContact(@RequestBody Contact contact) throws URISyntaxException {

        Contact contactSaved = contactService.saveContact(contact);
        String idContact = contactSaved.getEmail();

        return ResponseEntity.created(new URI("/api/contact/" + idContact)).body(contactSaved);
    }
    
    @PutMapping("/{id}")
    public ResponseEntity<Contact> saveContact(@RequestBody Contact contact, @PathVariable String id)
            throws URISyntaxException {
        Contact updateContact = contactService.updateContact(id, contact);
        return ResponseEntity.ok(updateContact);

    }
    
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Contact> getAllAContacts(){
        return contactService.getAllContacts();
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletarLancamento(@PathVariable String id) {

        Optional<Contact> contact = contactService.findByIdContact(id);

        if (!contact.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        contactService.deleteContact(id);
        return ResponseEntity.noContent().build();
    }

}
